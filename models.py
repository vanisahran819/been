from django.db import models

# Create your models here.

from django.db import models

class GLCM_FEATURE(models.Model):
    nama = models.CharField('nama' , max_length=200)
    kondisi = models.CharField('kondisi' , max_length=200)
    con1 = models.CharField('con1' , max_length=200)
    con2 = models.CharField('con2' , max_length=200)
    con3 = models.CharField('con3' , max_length=200)
    con4 = models.CharField('con4' , max_length=200)
    dis1 = models.CharField('dis1' , max_length=200)
    dis2 = models.CharField('dis2' , max_length=200)
    dis3 = models.CharField('dis3' , max_length=200)
    dis4 = models.CharField('dis4' , max_length=200)
    hom1 = models.CharField('hom1' , max_length=200)
    hom2 = models.CharField('hom2' , max_length=200)
    hom3 = models.CharField('hom3' , max_length=200)
    hom4 = models.CharField('hom4' , max_length=200)
    en1 = models.CharField('en1' , max_length=200)
    en2 = models.CharField('en2' , max_length=200)
    en3 = models.CharField('en3' , max_length=200)
    en4 = models.CharField('en4' , max_length=200)
    cor1 = models.CharField('cor1' , max_length=200)
    cor2 = models.CharField('cor2' , max_length=200)
    cor3 = models.CharField('cor3' , max_length=200)
    cor4 = models.CharField('cor4' , max_length=200)

