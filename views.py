
import sqlite3
import pandas as pd
import numpy as np
import skimage
import cv2
from django.conf.urls.static import static
from django.shortcuts import render
from skimage.feature import greycomatrix, greycoprops
from skimage import io, color, img_as_ubyte

# Import Gaussian Naive Bayes model
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import ComplementNB

# Import train_test_split function
from sklearn.model_selection import train_test_split


from .models import GLCM_FEATURE
from PIL import Image
from django.http import HttpResponse
from GLCM import settings

# GLCM properties
def contrast_feature(matrix_coocurrence):
    contrast = skimage.feature.greycoprops(matrix_coocurrence, 'contrast')
    return contrast

def read_transparent_png(filename):
    image_4channel = cv2.imread('static/image_to_train.png', cv2.IMREAD_UNCHANGED)

    alpha_channel = image_4channel[:,:,3]
    rgb_channels = image_4channel[:,:,:3]

    # White Background Image
    white_background_image = np.ones_like(rgb_channels, dtype=np.uint8) * 255

    # Alpha factor
    alpha_factor = alpha_channel[:,:,np.newaxis].astype(np.float32) / 255.0
    alpha_factor = np.concatenate((alpha_factor,alpha_factor,alpha_factor), axis=2)

    # Transparent Image Rendered on White Background
    base = rgb_channels.astype(np.float32) * alpha_factor
    white = white_background_image.astype(np.float32) * (1 - alpha_factor)
    final_image = base + white
    return final_image.astype(np.uint8)

def dissimilarity_feature(matrix_coocurrence):
	dissimilarity = skimage.feature.greycoprops(matrix_coocurrence, 'dissimilarity')
	return dissimilarity

def homogeneity_feature(matrix_coocurrence):
	homogeneity = skimage.feature.greycoprops(matrix_coocurrence, 'homogeneity')
	return homogeneity

def energy_feature(matrix_coocurrence):
	energy = skimage.feature.greycoprops(matrix_coocurrence, 'energy')
	return energy

def correlation_feature(matrix_coocurrence):
	correlation = skimage.feature.greycoprops(matrix_coocurrence, 'correlation')
	return correlation


def handle_uploaded_file(f):
    with open('static/image_to_train.png', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def handle_uploaded_file_gray(f):
    with open('static/image_to_train_gray.png', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def index(request):
    if request.method == 'POST':


        handle_uploaded_file(request.FILES['image'])
        filepath = static('image_to_train.png')
        img = read_transparent_png(filepath)  # shape is [416, 554, 3]

        img_grayscale = cv2.imread("static/image_to_train.png", 0)

        down_width = 320
        down_height = 320
        down_points = (down_width, down_height)
        img_grayscale = cv2.resize(img_grayscale, down_points, interpolation=cv2.INTER_LINEAR)

        cv2.imwrite('static/image_to_train_gray.png', img_grayscale)


        gray = color.rgb2gray(img)
        image = img_as_ubyte(gray)



        bins = np.array([0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240, 255])  # 16-bit
        inds = np.digitize(image, bins)

        max_value = inds.max() + 1
        matrix_coocurrence = skimage.feature.greycomatrix(inds, [1], [0, np.pi / 4, np.pi / 2, 3 * np.pi / 4], levels=max_value,
                                          normed=False, symmetric=False)

        print("---Start---")
        print(dissimilarity_feature(matrix_coocurrence))
        print("---End---")
        context = {
            'grayscale' : 'image_to_train.png',
            'contrast': contrast_feature(matrix_coocurrence)[0],
            'dissimilarity': dissimilarity_feature(matrix_coocurrence)[0],
            'homogeneity': homogeneity_feature(matrix_coocurrence)[0],
            'energy': energy_feature(matrix_coocurrence)[0],
            'correlation': correlation_feature(matrix_coocurrence)[0],

        }

        con = sqlite3.connect("glcm.db")
        cur = con.cursor()

        con1 = float(contrast_feature(matrix_coocurrence)[0][0])
        con2 = float(contrast_feature(matrix_coocurrence)[0][1])
        con3 = float(contrast_feature(matrix_coocurrence)[0][2])
        con4 = float(contrast_feature(matrix_coocurrence)[0][3])

        dis1 = float(dissimilarity_feature(matrix_coocurrence)[0][0])
        dis2 = float(dissimilarity_feature(matrix_coocurrence)[0][1])
        dis3 = float(dissimilarity_feature(matrix_coocurrence)[0][2])
        dis4 = float(dissimilarity_feature(matrix_coocurrence)[0][3])

        hom1 = float(homogeneity_feature(matrix_coocurrence)[0][0])
        hom2 = float(homogeneity_feature(matrix_coocurrence)[0][1])
        hom3 = float(homogeneity_feature(matrix_coocurrence)[0][2])
        hom4 = float(homogeneity_feature(matrix_coocurrence)[0][3])

        en1 = float(energy_feature(matrix_coocurrence)[0][0])
        en2 = float(energy_feature(matrix_coocurrence)[0][1])
        en3 = float(energy_feature(matrix_coocurrence)[0][2])
        en4 = float(energy_feature(matrix_coocurrence)[0][3])

        cor1 = float(correlation_feature(matrix_coocurrence)[0][0])
        cor2 = float(correlation_feature(matrix_coocurrence)[0][1])
        cor3 = float(correlation_feature(matrix_coocurrence)[0][2])
        cor4 = float(correlation_feature(matrix_coocurrence)[0][3])

        nama = request.POST.get("nama" , "")
        kondisi = request.POST.get("kondisi" , "")

        # Create table
        cur.execute('''CREATE TABLE IF NOT EXISTS training_glcm_feature
                       (nama text, kondisi text, con1 text, con2 text, con3 text,  con4 text,
                       dis1 text, dis2 text, dis3 text,  dis4 text,
                       hom1 text, hom2 text, hom3 text, hom4 text,
                       en1 text, en2 text, en3 text, en4 text,
                       cor1 text, cor2 text, cor3 text, cor4 text
                       )''')

        cur.execute("INSERT INTO training_glcm_feature (nama,kondisi,con1,con2,con3,con4,dis1,dis2,dis3,dis4,hom1,hom2,hom3,hom4,en1,en2,en3,en4,cor1,cor2,cor3,cor4) VALUES (?,? ,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",(nama , kondisi, con1, con2,con3,con4,dis1,dis2,dis3,dis4,hom1,hom2,hom3,hom4,en1,en2,en3,en4,cor1,cor2,cor3,cor4))


        con.commit()
        con.close()

        return render(request, 'training2/index.html' , context)
    else:
        return render(request, 'training2/index.html')

def cari(request):
    return "Hello World"

def testing(request):
    if request.method == 'POST':
        # Read sqlite query results into a pandas DataFrame
        handle_uploaded_file(request.FILES['image'])
        filepath = static('image_to_train.png')
        img = read_transparent_png(filepath)  # shape is [416, 554, 3]

        img_grayscale = cv2.imread("static/image_to_train.png", 0)

        cv2.imwrite('static/image_to_train_gray.png', img_grayscale)

        gray = color.rgb2gray(img)
        image = img_as_ubyte(gray)

        bins = np.array([0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240, 255])  # 16-bit
        inds = np.digitize(image, bins)

        max_value = inds.max() + 1
        matrix_coocurrence = skimage.feature.greycomatrix(inds, [1], [0, np.pi / 4, np.pi / 2, 3 * np.pi / 4], levels=max_value,
                                          normed=False, symmetric=False)

        con1 = float(contrast_feature(matrix_coocurrence)[0][0])
        con2 = float(contrast_feature(matrix_coocurrence)[0][1])
        con3 = float(contrast_feature(matrix_coocurrence)[0][2])
        con4 = float(contrast_feature(matrix_coocurrence)[0][3])

        dis1 = float(dissimilarity_feature(matrix_coocurrence)[0][0])
        dis2 = float(dissimilarity_feature(matrix_coocurrence)[0][1])
        dis3 = float(dissimilarity_feature(matrix_coocurrence)[0][2])
        dis4 = float(dissimilarity_feature(matrix_coocurrence)[0][3])

        hom1 = float(homogeneity_feature(matrix_coocurrence)[0][0])
        hom2 = float(homogeneity_feature(matrix_coocurrence)[0][1])
        hom3 = float(homogeneity_feature(matrix_coocurrence)[0][2])
        hom4 = float(homogeneity_feature(matrix_coocurrence)[0][3])

        en1 = float(energy_feature(matrix_coocurrence)[0][0])
        en2 = float(energy_feature(matrix_coocurrence)[0][1])
        en3 = float(energy_feature(matrix_coocurrence)[0][2])
        en4 = float(energy_feature(matrix_coocurrence)[0][3])

        cor1 = float(correlation_feature(matrix_coocurrence)[0][0])
        cor2 = float(correlation_feature(matrix_coocurrence)[0][1])
        cor3 = float(correlation_feature(matrix_coocurrence)[0][2])
        cor4 = float(correlation_feature(matrix_coocurrence)[0][3])

        new_data_point = np.array([con1,con2,con3,con4,dis1,dis2,dis3,dis4,hom1,hom2,hom3,hom4,en1,en2,en3,en4,cor1,cor2,cor3,cor4])

        con = sqlite3.connect("glcm.db")
        realdf = pd.read_sql_query("SELECT * from training_glcm_feature", con)
        df = realdf
        df = df.drop('nama' , axis=1)
        df = df.drop('kondisi' , axis=1)
        df = df.drop('id' , axis=1)
       # Verify that result of SQL query is stored in the dataframe
        print(realdf["kondisi"])
        print(df)


        # Split dataset into training set and test set
        X_train, X_test, y_train, y_test = train_test_split(df, realdf["kondisi"], test_size=0.3,
                                                            random_state=109)  # 70% training and 30% test
        # Create a Gaussian Classifier
        gnb = GaussianNB()
        bnb = ComplementNB()

        # Train the model using the training sets
        gnb.fit(X_train, y_train)
        bnb.fit(X_train, y_train)

        print("ini test")
        print(X_test)
        print(pd.DataFrame(new_data_point.reshape(1,-1)))
        # Predict the response for test dataset
        data = pd.DataFrame(new_data_point.reshape(1,-1))
        y_pred = gnb.predict(data)
        y_pred2 = bnb.predict(data)

        print(y_pred)
        print(y_pred2)

        con.close()
        context = {
            'grayscale': 'static/image_to_train.png',
            'contrast': contrast_feature(matrix_coocurrence)[0],
            'dissimilarity': dissimilarity_feature(matrix_coocurrence)[0],
            'homogeneity': homogeneity_feature(matrix_coocurrence)[0],
            'energy': energy_feature(matrix_coocurrence)[0],
            'correlation': correlation_feature(matrix_coocurrence)[0],
            'hasil' : y_pred,
            'hasil2': y_pred2

        }

        return render(request, 'training2/testing.html' , context)
    else:
        return render(request, 'training2/testing.html')

def data(request):
    data = GLCM_FEATURE.objects.all()

    return render(request, 'training2/data.html' , {'data' : data})

def hapusdata(request):
    GLCM_FEATURE.objects.filter(id=request.GET.get("id", "")).delete()
    data = GLCM_FEATURE.objects.all()

    return render(request, 'training/data.html' , {'data' : data})

def get_minvalue(inputlist):
    # get the minimum value in the list
    min_value = min(inputlist)

    # return the index of minimum value
    min_index = inputlist.index(min_value)
    return min_index